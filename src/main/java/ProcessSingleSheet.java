import jxl.Cell;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.read.biff.SheetImpl;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;

public class ProcessSingleSheet {

    private static final String FILE_PATH = "C:/Users/xiaoxma/Desktop/excel/singlesheet/all-groups.xls";


    public static void main(String[] args) throws IOException, BiffException {

        FileInputStream dataFileInputStream = new FileInputStream(FILE_PATH);
        HSSFWorkbook dataHssfWorkbook = new HSSFWorkbook(dataFileInputStream);
        HSSFSheet coverageApplicabilitySheet = dataHssfWorkbook.getSheetAt(0);

        int rowNum = ((HSSFSheet)coverageApplicabilitySheet).getLastRowNum();
        if ((rowNum+1)%2 != 0){
            System.out.println("Wrong row number");
            return;
        }
        DecimalFormat df = new DecimalFormat("0.000");

        for (int i=0;i<rowNum;){
            try {
                HSSFCell cellC = coverageApplicabilitySheet.getRow(i).getCell(2);
                String actualValueC = cellC.getStringCellValue();
                HSSFCell cellD = coverageApplicabilitySheet.getRow(i).getCell(3);
                String actualValueD = df.format(cellD.getNumericCellValue());
                HSSFCell cellE = coverageApplicabilitySheet.getRow(i).getCell(4);
                String actualValueE = df.format(cellE.getNumericCellValue());
                HSSFCell cellG = coverageApplicabilitySheet.getRow(i).getCell(6);
                String actualValueG = df.format(cellG.getNumericCellValue());
                HSSFCell cellH = coverageApplicabilitySheet.getRow(i).getCell(7);
                String actualValueH = df.format(cellH.getNumericCellValue());
                String s1 = actualValueC+actualValueD+actualValueE+actualValueG+actualValueH;

                HSSFCell cellCc = coverageApplicabilitySheet.getRow(i+1).getCell(2);
                String actualValueCc = cellCc.getStringCellValue();
                HSSFCell cellDd = coverageApplicabilitySheet.getRow(i+1).getCell(3);
                String actualValueDd = df.format(cellDd.getNumericCellValue());
                HSSFCell cellEe = coverageApplicabilitySheet.getRow(i+1).getCell(4);
                String actualValueEe = df.format(cellEe.getNumericCellValue());
                HSSFCell cellGg = coverageApplicabilitySheet.getRow(i+1).getCell(6);
                String actualValueGg = df.format(cellGg.getNumericCellValue());
                HSSFCell cellHh = coverageApplicabilitySheet.getRow(i+1).getCell(7);
                String actualValueHh = df.format(cellHh.getNumericCellValue());
                String s2 = actualValueCc+actualValueDd+actualValueEe+actualValueGg+actualValueHh;

                if (s1.equals(s2)){
                    i=i+2;
                    continue;
                }else {
                    System.out.println(i);
                    return;
                }
            }catch (Exception e){
                System.out.println(i);
            }

        }

        System.out.println("success=============================================================success");
    }
}
