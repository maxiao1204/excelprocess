import jxl.Cell;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProcessExcelMain {

    private static final String FILE_PATH = "D:/ExcelFiles";
    private static final String NOT_ALLOWED = "Not Allowed(不顯示)";
    private static final String OPTIONAL = "Optional(預選是，可修改)";
    private static final String OPTIONAL_FINAL = "optional";
    private static final String ON_DEMAND = "On Demand(預選否，可修改)";
    private static final String ON_DEMAND_FINAL = "onDemand";
    private static final String NOT_AVAILABLE = "notAvailable";

    public static void main(String[] args) throws IOException, BiffException {


        String defFileName = FILE_PATH+"/default.xls";
        String file1 = FILE_PATH+"/05.xls";

        // get default work book
        File defaultFile = new File(defFileName);
        Workbook defaultWorkbood = Workbook.getWorkbook(defaultFile);

        for (int i=11;i<=15;i++){ // default 05-30 columns.
            Cell[] column = defaultWorkbood.getSheet(3).getColumn(i); // get Components Applicability work sheet.
            FileInputStream dataFileInputStream = new FileInputStream(file1);
            HSSFWorkbook dataHssfWorkbook = new HSSFWorkbook(dataFileInputStream);
            dataFileInputStream.close();

            HSSFSheet coverageApplicabilitySheet = dataHssfWorkbook.getSheetAt(0);

//            HSSFWorkbook hssfWorkbook = new HSSFWorkbook();

            for (int j=7;j<72;j++){ // column value in each default column from 05 to 30
                String contents = column[j].getContents();
                HSSFRow row = coverageApplicabilitySheet.getRow(j-3); // get each row from sheet first and then column.
                if (NOT_ALLOWED.equals(contents)){
                    row.getCell(2).setCellValue(NOT_AVAILABLE); // cell 是每个单元格，这里是为该单元格赋值
                    processDataFile(dataHssfWorkbook, j, row);
                }else if (OPTIONAL.equals(contents)){
                    row.getCell(2).setCellValue(OPTIONAL_FINAL);
                }else if (ON_DEMAND.equals(contents)){
                    row.getCell(2).setCellValue(ON_DEMAND_FINAL);
                }
            }

            FileOutputStream fileOutputStream = new FileOutputStream("D:\\ExcelFiles\\changed\\Voluntary-08252013-08252013-"+(i-2)+".xls");
            dataHssfWorkbook.write(fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();

        }
    }

    static void processDataFile(HSSFWorkbook dataWorkBook, int j, HSSFRow row) throws IOException {

        OutputStream out = null;

        // get Component name from each cell
        String componentName = row.getCell(1).getStringCellValue();

        // process Default Value work sheet
        HSSFSheet defaultValueSheet = dataWorkBook.getSheetAt(1);
        // iterator each row in Default Value sheet and delete notAvailable row.
        for (int m=4;m<=defaultValueSheet.getLastRowNum();m++){
            HSSFRow defaultValueSheetRow = defaultValueSheet.getRow(m);
            if (defaultValueSheetRow!=null && componentName.equals(defaultValueSheetRow.getCell(1).getStringCellValue())){
                defaultValueSheet.removeRow(defaultValueSheetRow);
            }
        }

        // process Available Value sheet
        HSSFSheet availableValueSheet = dataWorkBook.getSheetAt(2);
        // iterator each row in Available Value sheet and delete notAvailable row.
        for (int m=4;m<=availableValueSheet.getLastRowNum();m++){
            HSSFRow availableValueSheetRow = availableValueSheet.getRow(m);
            if (availableValueSheetRow!=null && componentName.equals(availableValueSheetRow.getCell(1).getStringCellValue())){
                availableValueSheet.removeRow(availableValueSheetRow);
            }
        }

        // process Relationship sheet
        HSSFSheet relationshipSheet = dataWorkBook.getSheetAt(3);
        // iterator each row in Available Value sheet and delete notAvailable row.
        for (int m=4;m<=relationshipSheet.getLastRowNum();m++){
            HSSFRow relationshipSheetRow = relationshipSheet.getRow(m);
            if (relationshipSheetRow!=null && (componentName.equals(relationshipSheetRow.getCell(1).getStringCellValue())||componentName.equals(relationshipSheetRow.getCell(3).getStringCellValue()))){
                relationshipSheet.removeRow(relationshipSheetRow);
            }
        }

    }
}
